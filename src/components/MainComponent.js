import React, { Component } from 'react';
import Menu from './MenuComponent';
import { PLATILLOS } from '../shared/platillos';
import PlatoDetalle from "./PlatoDetalleComponent";
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import {Routes, Route, Navigate} from 'react-router-dom';

class Main extends Component {
    constructor(props){
        super(props);
        this.state={
            platos: PLATILLOS
        };
    }

    render(){
        const HomePage=() => {
            return(
                <Home/>
            );
        }

        return (
            <div>
                <Header/>
                <Routes> 
                    <Route path='/home' component={HomePage}/>
                    <Route exact path='/menu' component={() => <Menu platos={this.state.platos}/>}/>
                    <Route to='/home'/>
                </Routes>
                <Footer />
            </div>
        );
    }
}

export default Main;
