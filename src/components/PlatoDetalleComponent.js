import React from "react";
import {Card, CardImg, CardText, CardBody, CardTitle} from "reactstrap";

function RenderPlato({plato}){
    return(
        <div className="col-12 col-md-5 m-1">
            <Card className="col-12 col-md-5 m-1">
                <CardImg width="100%" src={plato.image} alt={plato.name} />
                <CardBody>
                    <CardTitle>{plato.name}</CardTitle>
                    <CardText>{plato.description}</CardText>
                </CardBody>
            </Card>
        </div>
    );
}
function RenderComentarios({comments}){
    if(comments !=null)
        return(
            <div className="col-12 col-md-5 m-1">
                <h2>Comentarios</h2>
                <ul className="list-unstyled">
                    {comments.map((comment)=>{
                        return(
                            <div key={comment.id}>
                                <li>
                                    <p>{comment.comment}</p>
                                    <p>{comment.rating}</p>
                                    <p>{comment.author}, {new Intl.DateTimeFormat("en-Us", {year: "numeric", month: "short", day: "2-digit"}).format(new Date (Date.parse(comment.date)))}</p>
                                </li>
                            </div>
                        );
                    })}
                </ul>
            </div>
        );
}

const PlatoDetalle=(props) => {
    if(props.plato !=null)
        return(
            <div className="container">
                <div className="row">
                    <RenderPlato plato={props.plato}/>
                    <RenderComentarios comments={props.plato.comments}/>
                </div>
            </div>
        );
    else
        return(
            <div></div>
        );
}
export default PlatoDetalle;