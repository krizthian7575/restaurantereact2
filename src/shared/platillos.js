export const PLATILLOS =
    [
        {
        id: 7,
        name: 'Logo',
        image: 'assets/images/logo.jpg',
        category: 'Logo',
        label: 'Restaurante',
        price: '$$',
        description: 'Excelente restaurante con variedad en el menú',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "Excelente comida",
            author: "Roberth Armas",
            date: "2015-10-10T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 3,
            comment: "Rico, pero los precios son muy caros",
            author: "Andres Yanouch",
            date: "2000-06-11T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 3,
        name: 'Chef',
        image: 'assets/images/chef.jpg',
        category: 'Cocinero',
        label: 'Excelente',
        price: '450',
        description: 'Excelente chef de nuestro restaurante',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "Cocina muy rico",
            author: "Andres Vela",
            date: "2022-12-23T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 3,
            comment: "Rico, pero mucha sal en la comida",
            author: "Paul Jativa",
            date: "2001-05-31T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 0,
        name: 'Batido',
        image: 'assets/images/batido.jpg',
        category: 'postres',
        label: 'Frío',
        price: '2.50',
        description: 'Bebida de frutas muy deliciosa',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "Delicioso",
            author: "Juan Pérez",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 4,
            comment: "Rico, pero faltó un poco de crema",
            author: "Paul Jara",
            date: "2014-09-05T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 1,
        name:'Hamburguesa',
        image: 'assets/images/hamburguesa.jpg',
        category: 'rápida',
        label:'Caliente',
        price:'4.00',
        description:'La típica comida rápida americana',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "La mejor",
            author: "Carla A",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 4,
            comment: "Buenisima!!",
            author: "Jaime Pineda",
            date: "2014-09-05T17:57:28.556094Z"
            },
            {
            id: 2,
            rating: 3,
            comment: "Faltó salsa",
            author: "Miguel Acosta",
            date: "2015-02-13T17:57:28.556094Z"
            }
        ]
        },
        {
        id: 2,
        name: 'Café',
        image: 'assets/images/cafe.jpg',
        category: 'bebida',
        label: 'Caliente',
        price: '3.50',
        description: 'Café Americano de buena calidad',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "Delicioso, se siente la frescura de los granos recien molidos",
            author: "Anderson Mejia",
            date: "2019-02-20T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 2,
            comment: "Muy amargo el café",
            author: "Johannes Estrada",
            date: "2020-01-05T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 4,
        name: 'cupcake',
        image: 'assets/images/cupcake.jpg',
        category: 'postre',
        label: 'Al clima',
        price: '1.50',
        description: 'Excelente mezcla de sabores en la masa',
        comments: [
            {
            id: 0,
            rating: 2,
            comment: "La masa muy suave",
            author: "Andy Cartagena",
            date: "2009-04-12T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 4,
            comment: "Rico, pero mucha crema",
            author: "Paul Jativa",
            date: "2017-06-01T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 4,
        name: 'Dona',
        image: 'assets/images/dona.jpg',
        category: 'Postre',
        label: 'Al clima',
        price: '2',
        description: 'Dona con crema de fresa y chispas',
        comments: [
            {
            id: 0,
            rating: 3,
            comment: "Muchas chispas para mi gusto",
            author: "Roberto Gabela",
            date: "2011-10-17T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 5,
            comment: "Crema de fresas muy rica",
            author: "Mishel Arciniega",
            date: "2018-08-08T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 5,
        name: 'pollo',
        image: 'assets/images/pollo.jpg',
        category: 'Comida rapida',
        label: 'Caliente',
        price: '2.5',
        description: 'Pollo con papas fritas y salsa de champiñones',
        comments: [
            {
            id: 0,
            rating: 4,
            comment: "Rica salsa de champiñones",
            author: "Jon Cevallos",
            date: "2020-12-15T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 5,
            comment: "Delicioso sabor del pollo  ",
            author: "Bryan Espinosa",
            date: "2013-09-15T17:57:28.556094Z"
            }
        ]                        
        },
        {
        id: 6,
        name: 'Taco',
        image: 'assets/images/taco.jpg',
        category: 'Comida Mexicana',
        label: 'Caliente',
        price: '4.50',
        description: 'Deliciosos tacos con queso y carne de res',
        comments: [
            {
            id: 0,
            rating: 4,
            comment: "Delicioso sabor de la carne con la tortilla",
            author: "Jimena Zambrano",
            date: "2006-11-27T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 3,
            comment: "Mucha lechuga y muy poca carne en el taco",
            author: "Daniela Escobar",
            date: "2023-02-019T17:57:28.556094Z"
            }
        ]                        
        },
    ];